from django.db import models

from django.contrib.auth.models import AbstractUser, UserManager

class CustomUserManager(UserManager):
    pass

class CustomUser(AbstractUser):
	objects = CustomUserManager()
	nama_warung = models.CharField(max_length=50, default="")

	def __str__(self):
            return self.nama_warung

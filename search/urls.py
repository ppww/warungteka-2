from django.urls import path
from . import views

urlpatterns = [
    path('',views.search,name='search'),
    path('databarang/',views.getDataBarang,name='databarang'),
]

from django import forms
from .models import SearchBar, Warung

class LokasiChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
         return obj.address

class SearchForm(forms.Form):
    namaproduk = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control form-control-sm', 'placeholder': 'Nama produk, misal telur'}),
    )

    lokasi = LokasiChoiceField(
        label = 'Lokasi',
        queryset = Warung.objects.all(),
        to_field_name = 'address',
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm'}),
    )

    class Meta:
        model = SearchBar
        fields = ('namaproduk', 'lokasi',)
from django.test import TestCase, Client
from django.urls import resolve
from .views import search
from .models import SearchBar
from .forms import SearchForm
from addwarung.models import Barang, Warung
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import time
# from selenium.webdriver.common.action_chains import ActionChains

class SearchTekaTest(TestCase):
    #     GENERAL TESTS      #
    # def test_url_search_is_exist(self):
    #     response = Client().get('/search/')
    #     self.assertEqual(response.status_code, 200)
    #
    # def test_using_search_func(self):
    #     found = resolve('/search/')
    #     self.assertEqual(found.func, search)
    #
    # def test_using_search_template(self):
    #     response = Client().get('/search/')
    #     self.assertTemplateUsed(response,'search.html')

    #     TEST FOR MODELS      #
    def test_model_can_create_new(self):
        new_search = SearchBar.objects.create(namaProduk='mie instan',lokasi='Jakarta')
        counting_all_available_search = SearchBar.objects.all().count()
        self.assertEqual(counting_all_available_search, 1)

    #     TEST FOR FORMS     #
    # def test_search_post_success_and_render_the_result(self):
    #     warung = Warung(name='Abc', address='Jakarta')
    #     warung.save()
    #     response = Client().post('/search/', {'namaproduk': 'telur', 'lokasi': Warung.objects.get(address='Jakarta').address, 'newSearch' : ''})
    #     html_response = response.content.decode()
    #     self.assertIn('telur', html_response)
    #     self.assertIn('Jakarta', html_response)

    def test_form_saves_values_to_instance_user_on_save(self):
        warung = Warung(name='Abc', address='Jakarta')
        warung.save()
        current = SearchBar.objects.all().count()
        searchbar = SearchBar(namaProduk='telur', lokasi='Jakarta')
        form = SearchForm({'namaproduk': 'telur', 'lokasi': Warung.objects.get(address='Jakarta').address, 'newSearch' : ''})
        self.assertTrue(form.is_valid())
        if (form.is_valid()):
            search = SearchBar(
                namaProduk=form.data['namaproduk'],
                lokasi=form.data['lokasi']
            )
            self.assertIsNotNone(search)
            search.save()
            allObjects = SearchBar.objects.all().count()
            self.assertEqual(allObjects,current+1)

            cekqueryBarang = form.data['namaproduk']
            cekqueryLokasi = form.data['lokasi']
            self.assertEqual(cekqueryBarang, 'telur')
            self.assertEqual(cekqueryLokasi, 'Jakarta')

            # response = Client().get('/search/')
            # html_response = response.content.decode('utf8')
            # self.assertIn(cekqueryBarang, html_response)
            # self.assertIn(cekqueryLokasi, html_response)


    # def test_delete_search(self):
    #     warung = Warung(name='Abc', address='Jakarta')
    #     warung.save()
    #     response = Client().post('/search/', {'namaproduk': 'telur', 'lokasi': Warung.objects.get(address='Jakarta').address, 'newSearch' : ''})
    #     current = SearchBar.objects.all().count()
    #     self.assertEqual(current,1)
    #     response = Client().post('/search/', {'deleteSearch' : ''})
    #     new = SearchBar.objects.all().count()
    #     self.assertEqual(new, 0)

# class SearchFunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(SearchFunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(SearchFunctionalTest, self).tearDown()
#
#     def test_hover_button(self):
#         selenium = self.selenium
#         selenium.get('http://127.0.0.1:8000/search/')
#         time.sleep(5)
#         ganti = selenium.find_element_by_id('buttonCari')
#         hover = ActionChains(selenium).move_to_element(ganti)
#         hover.perform()
#         newbackground = selenium.find_element_by_id('buttonCari').value_of_css_property('background-color')
#         self.assertEqual(newbackground, "rgba(103, 128, 159, 1)")
#         newtextcolor = selenium.find_element_by_id('buttonCari').value_of_css_property('color')
#         self.assertEqual(newtextcolor, "rgba(255, 255, 255, 1)")

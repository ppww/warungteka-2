$(document).ready(function () {
    $('#id_namaproduk').keypress(function () {
        $('#result').html('');
        var searchField = $('#id_namaproduk').val();
        var expression = new RegExp(searchField, "i");
        $.getJSON('/search/databarang/', function(data) {
            // This is a shorthand Ajax function
            $.each(data, function (key, value) {
                if (value.namaBarang.search(expression) != -1){
                    $('#result').append('<li style="padding: 0.1rem">' + value.namaBarang + '</li>');
                }
            });
        });
    }, function () {
        $('#result').html('');
    });

    $('#result').on('click', 'li', function () {
        var click_text = $(this).text();
        $('#id_namaproduk').val(click_text);
        $("#result").html('');
    });

    $('#buttonCari').hover(function () {
        $('#buttonCari').css('background-color', '#67809F');
        $('#buttonCari').css('color', '#FFFFFF');
    },function () {
        $('#buttonCari').css('background', 'transparent');
        $('#buttonCari').css('color', '#2E3131');
    });
});
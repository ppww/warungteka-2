from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from .forms import SearchForm
from .models import SearchBar
from addwarung.models import *
from django.db.models import Q
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.decorators import login_required

@login_required(login_url='/warungteka/login/')
def search(request):
    if (request.method == 'POST') and ('newSearch' in request.POST):
        form = SearchForm(request.POST)
        if form.is_valid():
            search = SearchBar(
                namaProduk = form.data['namaproduk'],
                lokasi = form.data['lokasi']
                )
            search.save()
            searchObjects = SearchBar.objects.all()
            queryNama = form.data['namaproduk']
            queryLokasi = form.data['lokasi']
            barangFiltered = Barang.objects.filter(
                Q(name__icontains=queryNama) & Q(warung__address__icontains=queryLokasi))
            content = {'searchObjects': searchObjects,
                       'barangFiltered': barangFiltered,
                       'queryNama': queryNama,
                       'queryLokasi': queryLokasi,
                       'form': form}
            return render(request, 'search.html', content)
    elif (request.method == 'POST') and ('deleteSearch' in request.POST):
        SearchBar.objects.all().delete()
        form = SearchForm()
    else:
        form = SearchForm()

    searchObjects = SearchBar.objects.all()
    barangFiltered = Barang.objects.filter(Q(name__icontains='.0'))
    content = {'searchObjects': searchObjects,
               'barangFiltered': barangFiltered,
               'form': form}
    return render(request, 'search.html',content)

def getDataBarang(request):
    if request.method == 'GET':
        allBarang = Barang.objects.all()
        dataBarang = []
        namaBarang = []
        for i in allBarang:
            if(i.name not in namaBarang):
                namaBarang.append(i.name)
                dataBarang.append({
                    'namaBarang': i.name,
                })
        result = json.dumps(dataBarang, cls=DjangoJSONEncoder)
        return HttpResponse(content=result, content_type="application/json")
    return HttpResponse('[]')
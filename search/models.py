from django.db import models
from addwarung.models import Warung

class SearchBar(models.Model):
    namaProduk = models.CharField(max_length=100)
    lokasi = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
$(document).ready(function(){
    panggilnama();

    $('#filldetails').click(function(){
        addFields();
    });
    
    $('#tombol').hover(function(){
        $('#tombol').css('background-color','#C7F991');
    },function(){
        $('#tombol').css('background-color','#FDE3A7');
    });
});

function addFields(){
    var nyak = document.getElementById('isimember').children.length;
    $('#isimember').append('<div style="margin-top: 10px;"><input style="width:90%; border-radius: 5px;" type="text" name="member' + nyak + '" /></div>');
}


function panggilnama() {
    var searchURL = "api";
    var val = "";
    $.ajax({
        url : searchURL,
        success : function(data){
            $("#result").empty();
            val += "<h4>Warung yang sudah ada:</h4>";
            val += "<ul>";
            $.each(data,function(i, items){
                val += "<li>" + items.name + "</li>";
            })
            val += "</ul>";
            
            $("#result").append(val);
        },
    });
}

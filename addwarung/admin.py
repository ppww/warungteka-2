from django.contrib import admin

# Register your models here.
from .models import Warung, Barang

admin.site.register(Warung)
admin.site.register(Barang)

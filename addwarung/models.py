from django.db import models

class Warung(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)

    def __str__(self):
        return self.name + " " + self.address

class Barang(models.Model):
    name = models.CharField(max_length=200)
    warung = models.ForeignKey(Warung, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

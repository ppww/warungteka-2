from django import forms
from .models import Barang, Warung

class Warungform(forms.Form):
    name = forms.CharField(max_length=200)
    address = forms.CharField(max_length=200)

    class Meta:
        model = Warung
        fields = ('name','address')

class Barangform(forms.Form):
    name = forms.CharField(max_length=200)
    # warung = forms.ForeignKey(Warung, on_delete=forms.CASCADE)

    class Meta:
        model = Barang
        fields = ('name')

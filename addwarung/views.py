from django.shortcuts import render
from .models import Warung, Barang
from django.contrib.auth.decorators import login_required
from .forms import Warungform, Barangform
from django.http import JsonResponse

@login_required(login_url='/warungteka/login/')
def addwarung(request):
    if request.method == 'POST':
        form = Warungform(request.POST)

        if form.is_valid():
            warung = Warung(name=form.data['name'], address=form.data['address'])
            warung.save()
            for i in request.POST:
                if i.startswith('member'):
                    if(request.POST[i] != ''):
                        form = Barangform(request.POST)
                        Barang(name=form.data[i], warung=warung).save()
        return render(request, 'addwarung.html', {})
    return render(request, 'addwarung.html', {})

def getName(request):
    objek = Warung.objects.all()
    array = []
    jsondata = {
        'data' : []
    }
    for i in objek:
        tes = {}
        tes['name'] = i.name
        tes['address'] = i.address
        array.append(tes)
    return JsonResponse(array, safe = False)
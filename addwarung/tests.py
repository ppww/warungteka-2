from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from .models import Warung, Barang
from django.urls import reverse
import unittest
from django.http import HttpRequest

# Create your tests here.
class myTest(TestCase):
    def setUp(self):
        warung = Warung(name='asd', address='sda')
        warung.save()
        barang = Barang(name='coba', warung=warung)
        barang.save()

    def test_coba(self):
        self.warung = Warung.objects.get(name='asd')
        self.assertEqual(str(self.warung), 'asd sda')

    def test_try(self):
        self.barang = Barang.objects.get(name='coba')
        self.assertEqual(str(self.barang),'coba')

    
    def test_get_name(self):
        response = Client().get('/addwarung/api')
        self.assertEqual('[{"name": "asd", "address": "sda"}]', response.content.decode())

class LoginTestCase(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        User = get_user_model()
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

    def testLogin(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get(reverse('addwarung'))
        a = self.client.post(reverse('addwarung'),{'name':'asd', 'address':'kutek','member':'telur'})
        self.assertEqual(a.status_code, 200)
        self.assertEqual(response.status_code, 200)
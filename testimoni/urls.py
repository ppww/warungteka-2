from django.urls import path
from testimoni import views

urlpatterns = [
    path('', views.testi, name='testimoni'),
    path('deleteTesti/', views.deleteTesti, name="deleteTesti"),
    path('addTesti/', views.addTesti, name="addTesti")
]

from django.test import TestCase, Client
from django.urls import resolve, reverse
from testimoni import views, models
from django.utils import timezone
import json


# Create your tests here.
class TestTestimoni(TestCase):

    # Exist

    def test_testi_url_is_exist(self):
        response = Client().get('/testimoni/')
        self.assertEqual(response.status_code,200)

    def test_listwarung_url_is_exist(self):
        response = Client().get('/listwarung/')
        self.assertEqual(response.status_code,200)

    # Template

    def test_testi_template(self):
        response = Client().get('/testimoni/')
        self.assertTemplateUsed(response, 'testi.html')

    def test_listwarung_template(self):
        response = Client().get('/listwarung/')
        self.assertTemplateUsed(response, 'listwarung.html')

    def test_404_template(self):
        response = Client().get('/error/')
        self.assertTemplateUsed(response, '404.html')

    # View Function

    def test_testi_using_func(self):
        found = resolve('/testimoni/')
        self.assertEqual(found.func, views.testi)

    def test_listwarung_using_func(self):
        found = resolve('/listwarung/')
        self.assertEqual(found.func, views.listwarung)

    # def test_create_testimoni(self):
    #     Client().post('/testimoni/addTesti/', {'warung': "namawarung", 'pesan': "yuhuu"})
    #     json_data = json.loads(Client().get('/testimoni/addTesti').content)
    #     print(json_data)


    # def test_delete_testimoni(self):


    # Models

    def test_model_new_testimoni(self):
        prev = models.Testimoni.objects.all().count()
        models.Testimoni.objects.create(warung="namawarung", pesan="ya", waktu=timezone.localtime(timezone.now()).strftime("%Y-%m-%d %H:%M:%S"))
        current = models.Testimoni.objects.all().count()
        self.assertEqual(current, prev+1)

    # def test_data_displayed(self):
    #     response = Client().post('/testimoni/', {'warung': "namawarung", 'pesan': "yuhuu"})
    #     self.assertIn('yuhuu', response.content.decode())

    def test_testimoni_model_str(self):
        models.Testimoni.objects.create(user="aku",warung="namawarung", pesan="ok", waktu=timezone.localtime(timezone.now()).strftime("%Y-%m-%d %H:%M:%S"))
        cur = str(models.Testimoni.objects.get(warung="namawarung", pesan="ok"))
        self.assertEqual("aku on namawarung", cur)

    # def test_del_testimoni_request(self):
    #     response = Client().post('/testimoni/', {'warung': "namawarung", 'pesan': "yuhuu"})
    #     new_response = self.client.post(reverse('testimoni'))
    #     self.assertRedirects(new_response, reverse('testimoni'), status_code=302)

    # def test_testimoni_view_save(self):
    #     response = Client().post('/testimoni/', data = {
    #         'warung': "namawarung",
    #         'pesan': "yuhuu",
    #         })
    #     self.assertTrue(models.Testimoni.objects.filter(warung="namawarung").exists())

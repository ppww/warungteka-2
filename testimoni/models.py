from django.db import models
from django.utils import timezone
from addwarung.models import Warung

# Create your models here.
class Testimoni(models.Model):
    waktu = models.DateTimeField(max_length = 50)
    user = models.CharField(max_length = 100)
    warung = models.CharField(max_length = 50)
    pesan = models.TextField(max_length = 150)

    def __str__(self):
        return self.user + " on " + self.warung

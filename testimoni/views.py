from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.utils import timezone
from addwarung.models import *
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
import json

# Create your views here.
def testi(request):
    form = TestiForm()

    context = {
        'form' : form,
    }
    return render(request, 'testi.html', context)

def deleteTesti(request):
    if request.method == "POST":
        if 'id' in request.POST:
            try:
                testi = Testimoni.objects.get(id=request.POST['id'])
            except:
                return "Terjadi kesalahan saat menghapus testimoni!"
            testi.delete()
            return HttpResponse("Berhasil dihapus!")
    return HttpResponse("Gagal dihapus!")

def addTesti(request):
    dataFinal = []
    data = {}
    if request.method == "POST":
        form = TestiForm(request.POST)
        # if 'id' in request.POST:
        #     Testimoni.objects.get(id=request.POST['id']).delete()
        #     return redirect('/testimoni/')
        if form.is_valid():
            user = request.user.username
            if user == "":
                return redirect('/testimoni/')
            testi = Testimoni(
                waktu = timezone.localtime(timezone.now()).strftime("%Y-%m-%d %H:%M:%S"),
                user = user,
                warung = (Warung.objects.get(id=form.data['warung']).name),
                pesan = form.data['pesan']
                )
            testi.save()
            data = {}
            data['id'] = testi.id
            data['waktu'] = testi.waktu
            data['user'] = testi.user
            data['warung'] = testi.warung
            data['pesan'] = testi.pesan
            dataFinal.append(data)
            # Testimoni.objects.all().delete()

            # return redirect('/testimoni/')
            return JsonResponse(dataFinal, safe=False)
    # else:
    dataFinal.clear()
    testi = Testimoni.objects.all()
    for e in testi:
        data = {}
        data['id'] = e.id
        data['waktu'] = e.waktu
        data['user'] = e.user
        data['warung'] = e.warung
        data['pesan'] = e.pesan
        print(data)
        dataFinal.append(data)
    print(dataFinal)
    return JsonResponse(dataFinal, safe=False)

def listwarung(request):
    context = {
        'warung' : Warung.objects.all().values(),
        'barang' : Barang.objects.all().values(),
        }
    return render(request, 'listwarung.html', context)

def error404(request, exception=None):
    return render(request, '404.html')

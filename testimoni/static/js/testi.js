$(document).ready(function() {
    getTesti();
    $('#postTesti').on('submit', function(event){
        event.preventDefault();
        addTesti();
    });
    // $(".delete").click(delTesti);
});

function delTesti(){
    var id_number = $('.delete').attr('id');
    console.log(id_number);
    $.ajax({
        type: "POST",
        url: "deleteTesti/",
        data: {
            "id": id_number,
            'csrfmiddlewaretoken': window.CSRF_TOKEN,
        },
        success: function(msg){
            alert(msg);
            // console.log("a");
            getTesti();
            // console.log("b");
        }
    });
};

function addTesti() {
    console.log("create Testi is working!") // sanity check
    $.ajax({
        type : "POST", // http method
        url : "addTesti/", // the endpoint
        data : {
            'warung' : $('#warung option:selected').val(),
            'pesan' : $('#msg').val(),
            'csrfmiddlewaretoken': window.CSRF_TOKEN,
        }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            $('#msg').val(''); // remove the value from the input
            getTesti();
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! Terjadi masalah : "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

function getTesti() {
    $.ajax({
        type : "GET", // http method
        url : "addTesti/", // the endpoint

        // handle a successful response
        success : function(json) {
            $('#msg').val(''); // remove the value from the input
            $("#output").empty();
            let val = '';
            $.each(json, function(i, data) {
                // console.log(data);
                val +='<div class="col-12 m-auto m-3 p-2">';
                val +='<div class="card-body" >';
                val +='<h4 class="card-title" style="color: #FED153;">' + data.warung + '</h4>';
                val +='<h6 class="card-subtitle mb-2 text-muted">by ' + data.user + '</h6>';
                val +='<p class="card-text">' + data.pesan + '</p>';
                if (getUser === data.user) {
                    val +='<button onclick="delTesti()" type="POST" id="' + data.id + '" class="delete btn btn-primary mx-auto mt-2" style="font-size: 0.8rem;">Delete</button>';
                }
                // val +='{% if ' + getUser + ' == ' + data.user + ' %}';
                val +='</div></div>';
            })
            $("#output").append(val);
        },
    });
}

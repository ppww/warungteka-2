# Teka ppww

[![pipeline status](https://gitlab.com/ppww/warungteka-2/badges/master/pipeline.svg)](https://gitlab.com/ppww/warungteka-2/commits/master)
[![coverage report](https://gitlab.com/ppww/warungteka-2/badges/master/coverage.svg)](https://gitlab.com/ppww/warungteka-2/commits/master)


2019/2020 - Groupwork (TK) for PPW

## Pengenalan

- URL   : [warungTeka](https://warungteka.herokuapp.com/)
- URL NG  : [warungTeka NG](https://warungteka-ng.herokuapp.com/)

- Anggota Kelompok **KB02** :
    - Alghifari Taufan Nugroho  - 1806146846
    - Andrew Theodore Tjondrowidjojo - 1806133862
    - Muhammad Dahlan Yasadipura - 1806205193
    - Thami Endamora - 1806141460

## Tentang warungTeka

warungTeka hadir untuk membantu masyarakat dalam mencari kebutuhan rumah tangga. Pemilik warung dapat mendaftarkan warungnya dan menulis barang jualannya. Kemudian warungTeka akan membantu konsumen mencari bahan yang diinginkan dan letak warung yang menjualnya.

## Daftar Fitur

- Halaman Login dan Pendaftaran (Dikerjakan oleh Dahlan Muhammad Dahlan Yasadipura) | **App warungteka**
- Halaman Cari Barang (Dikerjakan oleh Thami Endamora) | **App search**
- Halaman Tambah Warung (Dikerjakan oleh Andrew Theodore Tjondrowidjojo) | **App addwarung**
- Halaman Testimoni dan List Warung (Dikerjakan oleh Alghifari Taufan Nugroho) | **App testimoni**

## Tambahan Fitur

- *Coming Soon*
